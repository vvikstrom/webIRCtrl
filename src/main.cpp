#include <ProjectHeader.h>
#include <IRrecord.h>
#include <Common.h>


volatile bool print_buffer = false;
volatile uint32_t now = 0;
volatile uint32_t start_time = 0;
volatile bool timeout = false;

volatile long time_buffer[64];
size_t buffer_index = 0;
const size_t BUFFER_SIZE_LONG = sizeof(time_buffer)/sizeof(long);
const size_t BUFFER_SIZE_BYTES = sizeof(time_buffer);
volatile bool pin_change = false;
int global_count = 0;


void setArray(volatile long *array, size_t size, long value) {
  for (size_t i = 0; i < size; i++) {
    array[i] = value;
  }
}

long getClosestStdTime(long d_time) {
  // Find the closest standard time delta between samples for NEC
  uint16_t min_diff_position = 0;
  long min_diff = NEC_TIMING_ARRAY[0];
  for (size_t i = 0; i < (sizeof(NEC_TIMING_ARRAY)/sizeof(int16_t)); i++) {
    uint16_t delta_time = abs(d_time - NEC_TIMING_ARRAY[i]);
    if (delta_time < min_diff) {
      min_diff = delta_time;
      min_diff_position = i;
    }
  }
  // Check that it's within the time tolerance
  if (abs(d_time - NEC_TIMING_ARRAY[min_diff_position]) > TOLERANCE) {
    return (long)d_time;
  }
  return (long)NEC_TIMING_ARRAY[min_diff_position];
}

void normalizeBufferTime(volatile long *buffer, size_t size) {
  // Copy input array for reference and to avoid accumulative errors
  long buffer_cpy[size];
  for (size_t i = 0; i < size; i++) {
    buffer_cpy[i] = buffer[i];  // Cuz screw memcpy
  }
  for (size_t i = 1; i < size; i++) {
    buffer[i] = getClosestStdTime(buffer_cpy[i] - buffer_cpy[i-1]);
  }
  buffer[0] = 0;
}

bool verifyBufferTime(volatile long *buffer, size_t size) {
  bool increasing_time = false;
  for (size_t i = 1; i < size; i++) {
    increasing_time = buffer[i] > buffer[i-1];
  }
  return increasing_time;
}

void inline pinChangeISR(void) {
  noInterrupts();
  now = micros();
  if (buffer_index == 0)
    start_time = now;
  timeout = abs(now - start_time) > RECEIVE_TIMEOUT;
  if ((buffer_index < BUFFER_SIZE_LONG) && !timeout ) {
      time_buffer[buffer_index] = now;
      buffer_index++;
  }
  else if (buffer_index == BUFFER_SIZE_LONG) {
      buffer_index = 0;
      detachInterrupt(digitalPinToInterrupt(IR_INPUT));
      print_buffer = true;
  }
  else if (timeout)
      buffer_index = 0;
  else
    interrupts();
}

void setup() {
  Serial.begin(BAUD_RATE);
  delay(INIT_WAIT);
  pinMode(IR_INPUT, INPUT);
  // Serial.println("Rersetting array");
  setArray(time_buffer, sizeof(time_buffer)/sizeof(long), 0);
  // Serial.println("Attaching interrupts");
  noInterrupts();
  attachInterrupt(digitalPinToInterrupt(IR_INPUT), pinChangeISR, CHANGE);
  interrupts();
}

void loop(void) {
    if (print_buffer) {
      Serial.println("\nORIGINAL ARRAY");
      for (size_t i = 0; i < BUFFER_SIZE_LONG; i++) {
        Serial.print("[");
        Serial.print(time_buffer[i]);
        Serial.print("] ");
      }
      // Serial.println("");
      // Serial.println(*time_buffer);
      // Serial.println(sizeof(time_buffer));
      // Serial.println("BUFFER_SIZE_LONG: ");
      // Serial.println("SAME AGAIN!");
      // for (size_t i = 0; i < BUFFER_SIZE_LONG; i++) {
      //   Serial.print("[");
      //   Serial.print(time_buffer[i]);
      //   Serial.print("] ");
      // }
      if (verifyBufferTime(time_buffer, BUFFER_SIZE_LONG))
        normalizeBufferTime(time_buffer, BUFFER_SIZE_LONG);
      else {
        Serial.println("\nTIMER OVERFLOW!");
        setArray(time_buffer, BUFFER_SIZE_LONG, 0);
      }
      Serial.println("\nOUTPUT ARRAY");
      for (size_t i = 0; i < BUFFER_SIZE_LONG; i++) {
        Serial.print("[");
        Serial.print(time_buffer[i]);
        Serial.print("] ");
      }
      Serial.println("");
      Serial.println("");
      noInterrupts();
      attachInterrupt(digitalPinToInterrupt(IR_INPUT), pinChangeISR, CHANGE);
      interrupts();
      print_buffer = false;
    }
    else if (timeout) {
      Serial.print("\nTIMEOUT!! Reseting buffer\n");
      for (size_t i = 0; i < BUFFER_SIZE_LONG; i++) {
        Serial.print("[");
        Serial.print(time_buffer[i]);
        Serial.print("] ");
      }
      Serial.print("\nResetting timeout\n");
      timeout = false;
    }
    else
      delay(1);

}
