#ifndef IR_RECORD_H
#define  IR_RECORD_H

#include <Arduino.h>
#define IR_INPUT D1

/* Time constants for the NEC protocol
Time in microseconds accoring to NEC standard
HIGHBIT (1):
|****|   1680 us  |
|    |____________|
0   560          2250 us

LOWBIT (0):
|****|560 |
|    |____|
0   560   1120 us
*/
const uint16_t NEC_BIT_TIME = 560;
const uint16_t TOLERANCE = 100;
const uint32_t RECEIVE_TIMEOUT = 108000;
const uint16_t NEC_TIMING_ARRAY[] =
  {
    9000,
    4500,
    (NEC_BIT_TIME*3),
    NEC_BIT_TIME
  };
#endif /* IR_RECORD_H */
