#ifndef COMMON_H
#define COMMON_H
#include <Arduino.h>

// nodeMCU (Arduino) constants
#define LED D0
#define BAUD_RATE 115200
#define INIT_WAIT 100
#define MAX_TIMER_VAL_32 0xFFFFFFFF

// Server constants
const uint16_t PORT = 80;
const char* SSID = "Tage";
const char* PASSW = "W1llYs747AB";

#endif /* COMMON_H */
