#include <Arduino.h>
#include <ESP8266WiFi.h>
// #include <aREST.h>
// #include <IRremoteESP8266.h>
// #include <IRsend.h>

// nodeMCU (Arduino) constants
#define LED D0
#define IR_LED 4  // GPIO 4 (D2)
#define IR_INPUT D1
#define DEBUG_FLAG 1
#define BAUD_RATE 115200
#define INIT_WAIT 100
#define MAX_TIMER_VAL_32 0xFFFFFFFE


/* Time constants for the NEC protocol
Time in microseconds accoring to NEC standard
HIGHBIT (1):
|****|   1680 us  |
|    |____________|
0   560          2250 us

LOWBIT (0):
|****|560 |
|    |____|
0   560   1120 us
*/
const uint16_t NEC_BIT_TIME = 560;
const uint16_t TOLERANCE = 100;
const uint32_t RECEIVE_TIMEOUT = 108000;
const uint16_t NEC_TIMING_ARRAY[] =
  {
    9000,
    4500,
    (NEC_BIT_TIME*3),
    NEC_BIT_TIME
  };


// Server constants
const uint16_t PORT = 80;
const char* SSID = "Tage";
const char* PASSW = "W1llYs747AB";
