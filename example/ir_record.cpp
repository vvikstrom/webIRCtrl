#ifndef PROJECTHEADER
  #include <ProjectHeader.h>
#endif

volatile uint32_t buffer[500];
volatile int16_t buffer_index = 0;
volatile bool pin_change = false;

void resetArray(volatile uint32_t array[], uint32_t value = 0) {
  for (size_t i = 0; i < sizeof(array); i++) {
    array[i] = value;
  }
}

void inline pinChangeISR(void) {
  noInterrupts();
  pin_change = true;
  interrupts();
}

void setup() {
  Serial.begin(BAUD_RATE);
  delay(INIT_WAIT);
  pinMode(IR_INPUT, INPUT);
  resetArray(buffer);
  noInterrupts();
  attachInterrupt(digitalPinToInterrupt(IR_INPUT), pinChangeISR, CHANGE);
  interrupts();
}

void loop(void) {
  if (pin_change) {
    buffer[buffer_index] = micros();
    pin_change = false;
  }
  else {
    delay(2000);
    for (size_t i = 0; i < sizeof(buffer); i++) {
      Serial.print(buffer[i]);
    }
    Serial.println("");
  }
}
