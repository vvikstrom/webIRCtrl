#ifndef PROJECTHEADER
  #include <ProjectHeader.h>
#endif

#define CLOCK_SPEED_MHZ 80

uint32_t interval_us = 3000000;
uint16_t now = 0;

volatile bool isr_semaphore = false;

void pinToggle(int8_t led_pin) {
  digitalWrite(led_pin, !digitalRead(led_pin));
}

void flashLed(int8_t led_pin, int16_t repeat = 2, int16_t flash_time = 100) {
  for (int16_t i = 0; i < repeat; i++) {
    pinToggle(led_pin);
    delay(flash_time);
    pinToggle(led_pin);
    delay(flash_time);
  }
}

void inline timerISR(void) {
  noInterrupts();
  isr_semaphore = true;
  timer0_write(ESP.getCycleCount() + interval_us * CLOCK_SPEED_MHZ);
  interrupts();
}

void setup() {
  pinMode(LED, OUTPUT);
  digitalWrite(LED, 1);
  Serial.begin(BAUD_RATE);
  delay(INIT_WAIT);
  noInterrupts();
  timer0_isr_init();
  timer0_attachInterrupt(timerISR);
  timer0_write(ESP.getCycleCount() + interval_us * CLOCK_SPEED_MHZ);
  interrupts();
}

void loop(void) {
  if (isr_semaphore) {
    // now = micros();
    // Serial.print("ISR! Time: ");
    // Serial.println(now);
    isr_semaphore = false;
    flashLed(LED, 2, 100);
  }
  else {}
}
