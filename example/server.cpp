/*
  Simple web server for ESP8266 Node MCU.
  The MAC adress for my unit seems to be:
    5c:cf:7f:a3:13:8b
  That might come in handy..
*/
#ifndef PROJECTHEADER
  #include <ProjectHeader.h>
#endif

aREST rest = aREST();

WiFiServer server(PORT);

IRsend irsend(IR_LED);

// os_timer_t myTimer;

int data_s;
int no_of_times;
int key;

void debugPrint(const char* d_str) {
  if (DEBUG_FLAG) {
    Serial.println(d_str);
  }
  else return;
}

void flashLed(int16_t led_pin, int16_t repeat = 2, int16_t flash_time = 100) {
  for (int16_t i = 0; i < repeat; i++) {
    digitalWrite(led_pin, 1);
    delay(flash_time);
    digitalWrite(led_pin, 0);
    delay(flash_time);
  }
}

int ledCtrl(String arg) {
  flashLed(LED, arg.toInt());
  return 1;
}

int handleIr(String arg) {
  Serial.println(arg);
  debugPrint("DATA:");
  long data = arg.toInt();
  Serial.println(data);
  // irsend.sendNEC();
}

int recordIr() {
  return 1;
}

void setup() {
  pinMode(LED, OUTPUT);
  irsend.begin();
  Serial.begin(BAUD_RATE);
  delay(INIT_WAIT);

  data_s = 0;
  no_of_times = 5;
  rest.variable((char* )"data_s", &data_s);
  rest.variable((char* )"no_of_times", &no_of_times);

  rest.function((char *)"ledCtrl", ledCtrl);
  rest.function((char *)"handleIr", handleIr);

  rest.set_id((char *)"1");
  rest.set_name((char *)"nodeMCU");

  Serial.print("Connecting");
  WiFi.begin(SSID, PASSW);
  int16_t counter = 0;
  while (WiFi.status() != WL_CONNECTED) {
    flashLed(LED);
    Serial.print(".");
    delay(RECHECK_TIME);
    counter++;
    if (!(counter % 5)) Serial.println(WiFi.status());
  }

  Serial.println("Connected!");
  server.begin();
  Serial.println("Server started");
  Serial.println(WiFi.localIP());
}

void loop(void) {
  // server.handleClient();
  while (1) {
    flashLed(LED, 1, 1000);
  }
  // for (int i = 0; i < 10; i++) {
  //   irsend.sendNEC(BUTTON_1, 32);
  //   delay(2000);
// }

  // WiFiClient client = server.available();
  // if (!client)
  //   return;
  //
  // while(!client.available())
  //   delay(1);
  //
  // rest.handle(client);
  //yield();
}
